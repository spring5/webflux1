package spring5.webflux1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFlux1Application {

	public static void main(String[] args) {
		SpringApplication.run(WebFlux1Application.class, args);
	}
}
