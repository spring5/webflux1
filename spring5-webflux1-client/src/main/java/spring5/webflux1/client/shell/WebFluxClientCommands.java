/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spring5.webflux1.client.shell;

import java.net.URI;
import java.util.logging.Logger;
import org.springframework.http.MediaType;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import spring5.webflux1.domain.Greeting;

/**
 *
 * @author harry
 */
@ShellComponent
public class WebFluxClientCommands {

    private static final Logger logger = Logger.getLogger(WebFluxClientCommands.class.getName());

    @ShellMethod("Send GET request to the Greeting Service.")
    public String greet(@ShellOption(defaultValue = "World") String name) {

        int port = 7001;
        String url = String.format("http://localhost:%d", port);
        WebClient client = WebClient.create(url);

        String path = "/greeting";
        String targetUrl = url + path;        
        URI uri = UriComponentsBuilder.fromUriString(targetUrl).queryParam("name", name).build().toUri();
        logger.info(">>> uri = " + uri);

        Mono<Greeting> result = client.get()
                .uri(uri).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Greeting.class);

        Greeting greeting = result.block();
        String content = greeting.getContent();

        return content;
    }
}
