package spring5.webflux1.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@ComponentScan({"spring5.webflux1.domain", "spring5.webflux1.client", "spring5.webflux1.client.shell"})
public class WebFlux1ClientApplication {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        SpringApplication.run(WebFlux1ClientApplication.class, args);
    }
}
