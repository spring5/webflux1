# Getting Started (Gradle)


## Scaffold

    spring init --name=WebFlux1 -g=spring5 -a=webflux1 -d=webflux,devtools,actuator --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-webflux1
    spring init --name=WebFlux1Client -g=spring5 -a=webflux1.client -d=spring-shell,webflux,devtools --boot=2.0.0.BUILD-SNAPSHOT --build=gradle  spring5-webflux1-client



(*) List available dependencies

    spring init --list


## Build

    ./gradlew clean build



## Run

    ./gradlew bootRun





## References

* [Web on Reactive Stack](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html)
* [Spring Boot actuator endpoints](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/production-ready-endpoints.html#production-ready-endpoints-enabling-endpoints)

